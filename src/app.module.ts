import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BillModule } from './bill/bill.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { Bill } from './bill/entities/bill.entity';
import { member } from './member/entities/member.entity';
import { MemberModule } from './member/member.module';
import { ProductModule } from './product/product.module';
import { BranchModule } from './branch/branch.module';
import { Branch } from './branch/entities/branch.entity';
import { Product } from './product/entities/product.entity';
import { ReceiptModule } from './receipt/receipt.module';
import { TypesModule } from './types/types.module';
import { Type } from './types/entities/type.entity';
import { Receipt } from './receipt/entities/receipt.entity';
import { ReceiptDetail } from './receipt/entities/receiptDetail.entity';
import { Material } from './mathterial/entities/material.entity';
import { MaterialModule } from './mathterial/material.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { User } from './users/entities/user.entity';
import { Role } from './roles/entities/role.entity';
import { UsersModule } from './users/users.module';
import { RolesModule } from './roles/roles.module';
import { AuthModule } from './auth/auth.module';

import { Pay } from './payroll/entities/pay.entity';
import { PayModule } from './payroll/pay.module';
import { StockModule } from './stock/stock.module';
import { Stock } from './stock/entities/Stock.entity';
import { AttendanceModule } from './attendance/attendance.module';
import { Attendance } from './attendance/entities/attendance.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'mydb.sqlite',
      entities: [
        Bill,
        member,
        Branch,
        Product,
        Type,
        Receipt,
        ReceiptDetail,
        User,
        Role,
        Material,
        Pay,
        Stock,
        Attendance,
      ],
      synchronize: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    BillModule,
    MemberModule,
    ProductModule,
    BranchModule,
    ReceiptModule,
    MaterialModule,
    TypesModule,
    UsersModule,
    RolesModule,
    AuthModule,
    PayModule,
    StockModule,
    AttendanceModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
