import { Injectable } from '@nestjs/common';
import { CreateAttendanceDto } from './dto/create-attendance.dto';
import { UpdateAttendanceDto } from './dto/update-attendance.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Attendance } from './entities/attendance.entity';

@Injectable()
export class AttendanceService {
  constructor(
    @InjectRepository(Attendance)
    private attendanceRepository: Repository<Attendance>,
  ) {}
  create(createAttendanceDto: CreateAttendanceDto): Promise<Attendance> {
    return this.attendanceRepository.save(createAttendanceDto);
  }

  findAll(): Promise<Attendance[]> {
    return this.attendanceRepository.find();
  }

  findOne(id: number) {
    return this.attendanceRepository.findOneBy({ id: id });
  }

  async update(id: number, updateAttendanceDto: UpdateAttendanceDto) {
    await this.attendanceRepository.update(id, updateAttendanceDto);
    const attendance = await this.attendanceRepository.findOneBy({ id });
    return attendance;
  }

  async remove(id: number) {
    const deleteAttendance = await this.attendanceRepository.findOneBy({ id });
    return this.attendanceRepository.remove(deleteAttendance);
  }
}
