import { Injectable } from '@nestjs/common';
import { CreatePayDto } from './dto/create-pay.dto';
import { UpdatePayDto } from './dto/update-pay.dto';
import { Pay } from './entities/pay.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';


@Injectable()
export class PaysService {
  constructor(
    @InjectRepository(Pay) private paysRepository: Repository<Pay>,
    
  ) {}
  create(createPayDto: CreatePayDto) {
    const pay = new Pay();

    pay.approve = createPayDto.approve;
    pay.date = createPayDto.date;
    pay.total_salary= createPayDto.total_salary;
    pay.user = JSON.parse(createPayDto.userid);
    
    return this.paysRepository.save(pay); 
  }
  findAll() {
    return this.paysRepository.find({ relations: { user: true } });
  }

  findOne(id: number) {
    return this.paysRepository.findOneBy({id: id,});
  }

  async update(id: number, updatePayDto: UpdatePayDto) {
    const pay = new Pay();
    pay.approve = updatePayDto.approve;
    pay.date =  updatePayDto.date;
    pay.total_salary=  updatePayDto.total_salary;

    const updatePay = await this.paysRepository.findOneByOrFail({id: id,
      
});
    updatePay.approve = pay.approve;
    updatePay.date = pay.date;
    updatePay.total_salary = pay.total_salary;
 
    await this.paysRepository.save(updatePay);
   

    
    const result = await this.paysRepository.findOneBy({id: id,
     
    });
    return result;
  }

  async remove(id: number) {
    const deletePay = await this.paysRepository.findOneByOrFail({id: id,
    });
    await this.paysRepository.remove(deletePay);

    return deletePay;
  }
}
