import { Module } from '@nestjs/common';
import { PaysService } from './pay.service';
import { PaysController } from './pay.controller';
import { Pay } from './entities/pay.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Pay])],
  controllers: [PaysController],
  providers: [PaysService],
  exports: [PaysService],
})
export class PayModule {}
