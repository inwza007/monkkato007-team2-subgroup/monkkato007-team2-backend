
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Pay {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  approve: string;

  @Column()
  date: string;

  @Column()
  total_salary: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  
  @ManyToOne(() => User, (user) => user.id) 
  user: User; 
  
  

}
