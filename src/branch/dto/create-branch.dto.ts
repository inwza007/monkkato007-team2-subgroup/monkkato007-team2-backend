import { IsNotEmpty, Matches, MaxLength, MinLength } from 'class-validator';

export class CreateBranchDto {
  @IsNotEmpty()
  @MinLength(5)
  @MaxLength(225)
  nameStore: string;
  @IsNotEmpty()
  @MinLength(5)
  @MaxLength(225)
  address: string;
  @IsNotEmpty()
  @Matches(/^0[0-9]{9}$/)
  phone: string;
}
