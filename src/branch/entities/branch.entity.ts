import { Bill } from 'src/bill/entities/bill.entity';
import { Receipt } from 'src/receipt/entities/receipt.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class Branch {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  tel: string;

  @Column()
  addres: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => Bill, (bill) => bill.branch)
  bills: Bill[];

  @OneToMany(() => Receipt, (receipt) => receipt.branchs)
  receipts: Receipt[];
}
