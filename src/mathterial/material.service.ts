/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { CreateMaterialDto } from './dto/create-material.dao';
import { UpdateMaterialDto } from './dto/update-material.dao';
import { Material } from './entities/material.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MaterialService {
  constructor(
    @InjectRepository(Material)
    private materialRepository: Repository<Material>,
  ) {}
  create(createMaterialDto: CreateMaterialDto) {
    const material = new Material();
    material.material_name = createMaterialDto.material_name;
    material.material_min = parseFloat(createMaterialDto.material_min);
    material.material_unit = createMaterialDto.material_unit;
    material.material_max = parseFloat(createMaterialDto.material_max);
    material.material_price = parseFloat(createMaterialDto.material_price);
    return this.materialRepository.save(material);
  }

  findAll() {
    return this.materialRepository.find();
  }

  findOne(id: number) {
    return this.materialRepository.findOneBy({ material_id: id });
  }

  async update(id: number, updateMaterialDto: UpdateMaterialDto) {
    const material = new Material();
    material.material_name = updateMaterialDto.material_name;
    material.material_min = parseFloat(updateMaterialDto.material_min);
    material.material_unit = updateMaterialDto.material_unit;
    material.material_price = parseFloat(updateMaterialDto.material_price);
    const updateMaterial = await this.materialRepository.findOneOrFail({
      where: { material_id: id },
    });
    updateMaterial.material_name = material.material_name;
    updateMaterial.material_min = material.material_min;
    updateMaterial.material_unit = material.material_unit;
    updateMaterial.material_price = material.material_price;

    await this.materialRepository.save(updateMaterial);
    const result = await this.materialRepository.findOne({
      where: { material_id: id },
    });
    return result;
  }

  async remove(id: number) {
    const deleteMaterial = await this.materialRepository.findOneBy({
      material_id: id,
    });
    return this.materialRepository.remove(deleteMaterial);
  }
}
