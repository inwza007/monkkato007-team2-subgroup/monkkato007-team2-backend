export class CreateMaterialDto {
  material_name: string;
  material_min: string;
  material_max: string;
  material_unit: string;
  material_price: string;
}
