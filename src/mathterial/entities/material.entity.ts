import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@Entity()
export class Material {
  @PrimaryGeneratedColumn()
  material_id: number;

  @Column()
  material_name: string;

  @Column()
  material_min: number;

  @Column()
  material_max: number;

  @Column()
  material_unit: string;

  @Column()
  material_price: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;
}
