export class CreateProductDto {
  name: string;
  price: string;
  image: string;
  type: string;
  subType: string;
  size: string;
  sweetLevel: string;
}
