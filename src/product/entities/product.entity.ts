import { ReceiptDetail } from 'src/receipt/entities/receiptDetail.entity';
import { Type } from 'src/types/entities/type.entity';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'name',
    default: '',
  })
  name: string;

  @Column({
    name: 'price',
    default: 0,
  })
  price: number;

  @Column({
    name: 'typeId',
    default: 0,
  })
  typeId: number;

  @Column({
    name: 'subType',
    default: '',
  })
  subType: string;

  @Column({
    name: 'size',
    default: '',
  })
  size: string;

  @Column({
    name: 'sweetLevel',
    default: '',
  })
  sweetLevel: string;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Type, (type) => type.products)
  type: Type;

  @OneToMany(() => ReceiptDetail, (receiptDetail) => receiptDetail.product)
  receiptDetails: ReceiptDetail[];
}
