import { Module } from '@nestjs/common';
import { StockController } from './stock.controller';
import { StockService } from './stock.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Stock } from './entities/Stock.entity';
import { Branch } from 'src/branch/entities/branch.entity';
import { Material } from 'src/mathterial/entities/material.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Stock, Material, Branch])],
  controllers: [StockController],
  providers: [StockService],
})
export class StockModule {}
