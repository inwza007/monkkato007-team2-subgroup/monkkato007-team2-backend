import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { StockService } from './stock.service';
import { CreateStockDto } from './dto/create-stock.dto';
import { UpdateStockDto } from './dto/update-stock.dto';

@Controller('stock')
export class StockController {
    constructor(private readonly stockService: StockService) { }

    @Post()
    create(@Body() createStockDto: CreateStockDto) {
        return this.stockService.create(createStockDto);
    }

    @Get()
    findAll() {
        return this.stockService.findAll();
    }
    // Read One
    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.stockService.findOne(+id);
    }
    // Partial Update
    @Patch(':id')
    update(
        @Param('id') id: string,
        @Body() updateStockDto: UpdateStockDto,
    ) {
        return this.stockService.update(+id, updateStockDto);
    }
    // Delete
    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.stockService.remove(+id);
    }
}
