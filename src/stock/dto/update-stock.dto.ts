export class UpdateStockDto  {
    stock_qty: number;
    material_max: number;
    material_min: number;
    material_unit: string;
    material_status: string;
    materialId: number;
    branchId: number;
}
