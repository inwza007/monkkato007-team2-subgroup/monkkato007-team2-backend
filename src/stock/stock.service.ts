import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Stock } from './entities/Stock.entity';
import { Injectable } from '@nestjs/common';
import { CreateStockDto } from './dto/create-stock.dto';
import { UpdateStockDto } from './dto/update-stock.dto';
import { Material } from 'src/mathterial/entities/material.entity';
import { Branch } from 'src/branch/entities/branch.entity';

@Injectable()
export class StockService {
    constructor(
        @InjectRepository(Stock)
        private stockRepository: Repository<Stock>,
        @InjectRepository(Material)
        private materialRepository: Repository<Material>,
        @InjectRepository(Branch)
        private brachRepository: Repository<Branch>,
    ) { }
    async create(createStockDto: CreateStockDto): Promise<Stock> {
        const stock = new Stock()
        const branch = await this.brachRepository.findOneBy({
            id: createStockDto.branchId,
        });
        const material = await this.materialRepository.findOneBy({
            material_id: createStockDto.materialId,
        })
        stock.branch = branch;
        stock.material = material;
        stock.stock_qty = createStockDto.qty;
        if (stock.stock_qty < stock.material.material_max / 2 && stock.stock_qty > 0) {
            stock.material_status = "LOW STOCK"
        } else if (stock.stock_qty <= 0) {
            stock.material_status = "OUT OF STOCK"
        } else {
            stock.material_status = "FULL STOCK"
        }
        return this.stockRepository.save(stock);
    }

    findAll() {
        return this.stockRepository.find({ relations: { material: true, branch: true } });
    }

    findOne(id: number) {
        return this.stockRepository.findOne({ where: { stock_id: id }, relations: { material: true, branch: true } });
    }

    async update(id: number, updateStockDto: UpdateStockDto) {
        const stock = new Stock();
        const branch = await this.brachRepository.findOneBy({
            id: updateStockDto.branchId,
        });
        const material = await this.materialRepository.findOneBy({
            material_id: updateStockDto.materialId,
        })
        stock.stock_qty = updateStockDto.stock_qty;

        if (stock.stock_qty < stock.material.material_max / 2 && stock.stock_qty > 0) {
            stock.material_status = "Almost Out of Stock"
        } else if (stock.stock_qty <= 0) {
            stock.material_status = "Out of Stock"
        } else {
            stock.material_status = "Full Stock"
        }
        stock.branch = branch;
        stock.material = material;
        const updateStock = await this.stockRepository.findOneOrFail({ where: { stock_id: id } })
        updateStock.stock_qty = stock.stock_qty;
        updateStock.material = stock.material;
        updateStock.material_status = stock.material_status;
        updateStock.branch = stock.branch;

        await this.stockRepository.save(updateStock);
        const result = await this.stockRepository.findOne({
            where: { stock_id: id },
            relations: { material: true, branch: true }
        });
        return result;
    }

    async remove(id: number) {
        const deleteStock = await this.stockRepository.findOneBy({ stock_id: id });
        return this.stockRepository.remove(deleteStock);
    }
}
