import { Branch } from 'src/branch/entities/branch.entity';
import { Material } from 'src/mathterial/entities/material.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Stock {
  @PrimaryGeneratedColumn({
    name: 'stock_id'
  })
  stock_id: number;

  @Column({
    name: 'stock_qty',
    default: 0
  })
  stock_qty: number;

  @Column({
    name: 'material_status',
    default:''
  })
  material_status: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Material,{ cascade: true })
  material: Material;

  @ManyToOne(() => Branch, (branch) => branch.bills, { cascade: true })
  branch: Branch;
}
