import { Module } from '@nestjs/common';
import { PromotionsService } from './promotion.service';
import { PromotionsController } from './promotion.controller';
import { Promotion } from './entities/promotion.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Promotion])],
  controllers: [PromotionsController],
  providers: [PromotionsService],
})
export class PromotionsModule {}
