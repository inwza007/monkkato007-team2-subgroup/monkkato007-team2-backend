import { Injectable } from '@nestjs/common';
import { CreatePromotionDto } from './dto/create-promotion.dto';
import { UpdatePromotionDto } from './dto/update-promotion.dto';
import { Promotion } from './entities/promotion.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class PromotionsService {
  constructor(
    @InjectRepository(Promotion)
    private promotionRepository: Repository<Promotion>,
  ) {}
  create(createpromotionDto: CreatePromotionDto): Promise<Promotion> {
    return this.promotionRepository.save(createpromotionDto);
  }

  findAll(): Promise<Promotion[]> {
    return this.promotionRepository.find();
  }

  findOne(id: number) {
    return this.promotionRepository.findOneBy({ id: id });
  }

  async update(id: number, updatepromotionDto: UpdatePromotionDto) {
    await this.promotionRepository.update(id, updatepromotionDto);
    const promotion = await this.promotionRepository.findOneBy({ id });
    return promotion;
  }

  async remove(id: number) {
    const deletePromotion = await this.promotionRepository.findOneBy({ id });
    return this.promotionRepository.remove(deletePromotion);
  }
}
