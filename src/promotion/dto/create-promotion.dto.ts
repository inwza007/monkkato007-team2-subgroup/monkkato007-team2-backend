import { IsNotEmpty } from 'class-validator';

export class CreatePromotionDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  startdate: Date | null;

  @IsNotEmpty()
  enddate: Date | null;

  @IsNotEmpty()
  status: 'Active' | 'Inactive';

  @IsNotEmpty()
  discount: number;
}
