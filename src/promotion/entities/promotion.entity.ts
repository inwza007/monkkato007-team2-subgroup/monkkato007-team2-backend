import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Promotion {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  startdate: Date | null;

  @Column()
  enddate: Date | null;

  @Column()
  status: 'Active' | 'Inactive';

  @Column()
  discount: number;
}
