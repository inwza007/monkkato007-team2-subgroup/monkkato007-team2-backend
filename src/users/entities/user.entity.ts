import { Bill } from 'src/bill/entities/bill.entity';

import { Receipt } from 'src/receipt/entities/receipt.entity';
import { Role } from 'src/roles/entities/role.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column()
  fullName: string;

  @Column()
  gender: string;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => Receipt, (receipt) => receipt.user)
  receipts: Receipt[];

  @ManyToOne(() => Role, (role) => role.users)
  roles: Role;

  @OneToMany(() => Bill, (bill) => bill.user)
  bills: Bill[];
}
