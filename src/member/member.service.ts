import { Injectable } from '@nestjs/common';
import { CreateMemberDto } from './dto/create-member.dto';
import { UpdateMemberDto } from './dto/update-member.dto';
import { member } from './entities/member.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MemberService {
  constructor(
    @InjectRepository(member)
    private MemberRepository: Repository<member>,
  ) {}
  create(createMemberDto: CreateMemberDto): Promise<member> {
    createMemberDto.point = '0';
    createMemberDto.usedPoint = '0';
    return this.MemberRepository.save(createMemberDto);
  }

  findAll() {
    return this.MemberRepository.find();
  }

  findOne(id: number) {
    return this.MemberRepository.findOneBy({ id: id });
  }

  async update(id: number, updateUserDto: UpdateMemberDto) {
    await this.MemberRepository.update(id, updateUserDto);
    const user = await this.MemberRepository.findOneBy({ id });
    return user;
  }

  async remove(id: number) {
    const deleteMember = await this.MemberRepository.findOneBy({ id });

    return this.MemberRepository.remove(deleteMember);
  }
}
