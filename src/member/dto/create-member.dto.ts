export class CreateMemberDto {
  fullName: string;
  tel: string;
  email: string;
  password: string;
  point: string;
  birthday: string;
  usedPoint: string;
}
