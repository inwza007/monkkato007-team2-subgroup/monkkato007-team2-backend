import { Receipt } from 'src/receipt/entities/receipt.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class member {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  fullName: string;

  @Column()
  tel: string;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column()
  point: string;

  @Column()
  birthday: string;

  @Column()
  usedPoint: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => Receipt, (receipt) => receipt.members)
  receipts: Receipt[];
}
