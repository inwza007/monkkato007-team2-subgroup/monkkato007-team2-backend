import { Module } from '@nestjs/common';
import { MemberService } from './member.service';
import { MemberController } from './member.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { member } from './entities/member.entity';

@Module({
  imports: [TypeOrmModule.forFeature([member])],
  controllers: [MemberController],
  providers: [MemberService],
})
export class MemberModule {}
