import { Branch } from 'src/branch/entities/branch.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Bill {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  billType: string;

  @Column()
  image: string;

  @Column()
  detail: string;

  @Column()
  amount: number;

  @Column()
  userId: number;

  @Column()
  branchId: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Branch, (branch) => branch.bills, { cascade: true })
  branch: Branch;

  @ManyToOne(() => User, (user) => user.bills, { cascade: true })
  user: User;
}
