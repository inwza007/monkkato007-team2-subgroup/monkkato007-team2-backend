import { Injectable } from '@nestjs/common';
import { CreateBillDto } from './dto/create-bill.dto';
import { UpdateBillDto } from './dto/update-bill.dto';
import { Bill } from './entities/bill.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class BillService {
  constructor(
    @InjectRepository(Bill)
    private billRepository: Repository<Bill>,
  ) {}

  create(createbillDto: CreateBillDto): Promise<Bill> {
    return this.billRepository.save(createbillDto);
  }
  findAll(): Promise<Bill[]> {
    return this.billRepository.find();
  }

  findOne(id: number): Promise<Bill | null> {
    return this.billRepository.findOneBy({ id });
  }
  async update(id: number, updateBillDto: UpdateBillDto) {
    await this.billRepository.update(id, updateBillDto);
    const bill = await this.billRepository.update(id, updateBillDto);
    return bill;
  }
  async remove(id: number) {
    const deleteBill = await this.billRepository.findOneBy({ id });
    return this.billRepository.remove(deleteBill);
  }
}
