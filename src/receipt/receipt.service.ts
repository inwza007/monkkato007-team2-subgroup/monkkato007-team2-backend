import { Injectable } from '@nestjs/common';
import { CreateReceiptDto } from './dto/create-receipt.dto';
import { UpdateReceiptDto } from './dto/update-receipt.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Receipt } from './entities/receipt.entity';
import { ReceiptDetail } from './entities/receiptDetail.entity';
import { Product } from 'src/product/entities/product.entity';
import { Branch } from 'src/branch/entities/branch.entity';
import { User } from 'src/users/entities/user.entity';
import { member } from 'src/member/entities/member.entity';

@Injectable()
export class ReceiptService {
  constructor(
    @InjectRepository(Receipt) private receiptsRepository: Repository<Receipt>,
    @InjectRepository(ReceiptDetail)
    private receiptDetailsRepository: Repository<ReceiptDetail>,
    @InjectRepository(Product) private productsRepository: Repository<Product>,
    @InjectRepository(Branch) private branchsRepository: Repository<Branch>,
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(member) private membersRepository: Repository<member>,
  ) {}
  async create(createReceiptDto: CreateReceiptDto): Promise<Receipt> {
    const receipt = new Receipt();
    const user = await this.usersRepository.findOne({
      where: { id: createReceiptDto.userId },
    });
    const member = await this.membersRepository.findOne({
      where: { id: createReceiptDto.memberId },
    });
    receipt.qty = 0;
    receipt.total = 0;
    receipt.discount = 0;
    receipt.net = 0;
    receipt.amount = createReceiptDto.amount;
    receipt.Change = 0;
    receipt.payType = createReceiptDto.payType;
    receipt.branchsId = 1;
    receipt.user = user;
    receipt.members = member;
    receipt.membersId = createReceiptDto.memberId;
    receipt.receiptDetails = [];
    console.log(createReceiptDto.userId);
    console.log(receipt);
    console.log(createReceiptDto.receiptDetails);
    for (const oi of createReceiptDto.receiptDetails) {
      const receiptDetail = new ReceiptDetail();
      receiptDetail.product = await this.productsRepository.findOne({
        where: { id: oi.productId },
      });
      receiptDetail.name = receiptDetail.product.name;
      receiptDetail.price = receiptDetail.product.price;
      receiptDetail.qty = oi.qty;
      receiptDetail.total = receiptDetail.price * receiptDetail.qty;
      await this.receiptDetailsRepository.save(receiptDetail);
      receipt.receiptDetails.push(receiptDetail);
      receipt.total += receiptDetail.total;
      receipt.qty += receiptDetail.qty; // เพิ่มจำนวนสินค้าลงใน receipt
      receipt.Change = receipt.amount - receipt.total;
      receipt.net = receipt.total - receipt.discount;
    }
    return this.receiptsRepository.save(receipt);
  }

  findAll() {
    return this.receiptsRepository.find({
      relations: {
        receiptDetails: true,
        branchs: true,
        user: true,
        members: true,
      },
      order: { id: 'desc' },
    });
  }

  findOne(id: number) {
    return this.receiptsRepository.findOneOrFail({
      where: { id },
      relations: { receiptDetails: true, branchs: true, user: true },
    });
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  update(id: number, updateReceiptDto: UpdateReceiptDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const deleteOrder = await this.receiptsRepository.findOneOrFail({
      where: { id },
    });
    await this.receiptsRepository.remove(deleteOrder);

    return deleteOrder;
  }
}
