import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from 'typeorm';
import { Receipt } from './receipt.entity';
import { Product } from 'src/product/entities/product.entity';

@Entity()
export class ReceiptDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'name',
    default: '',
  })
  name: string;

  @Column({
    name: 'price',
    default: '',
  })
  price: number;

  @Column()
  qty: number;

  @Column()
  total: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Receipt, (receipt) => receipt.receiptDetails, {
    onDelete: 'CASCADE',
  })
  receipt: Receipt;

  @ManyToOne(() => Product, (product) => product.receiptDetails, {
    onDelete: 'CASCADE',
  })
  product: Product;
}
