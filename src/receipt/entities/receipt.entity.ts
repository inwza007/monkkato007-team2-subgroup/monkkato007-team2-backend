import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { ReceiptDetail } from './receiptDetail.entity';
import { User } from 'src/users/entities/user.entity';
import { member } from 'src/member/entities/member.entity';
import { Branch } from 'src/branch/entities/branch.entity';

@Entity()
export class Receipt {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  total: number;

  @Column()
  discount: number;

  @Column()
  net: number;

  @Column({
    name: 'amount',
    default: 0,
  })
  amount: number;

  @Column()
  payType: string;

  @Column()
  Change: number;

  @Column({
    name: 'qty',
    default: 0,
  })
  qty: number;

  @Column({
    name: 'branchsId',
    default: 1,
  })
  branchsId: number;

  @Column({
    name: 'membersId',
    nullable: true,
    default: 0,
  })
  membersId: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => ReceiptDetail, (receiptDetail) => receiptDetail.receipt)
  receiptDetails: ReceiptDetail[];

  @ManyToOne(() => User, (user) => user.receipts, { onDelete: 'CASCADE' })
  user: User;

  @ManyToOne(() => member, (member) => member.receipts, { onDelete: 'CASCADE' })
  members: member;

  @ManyToOne(() => Branch, (branch) => branch.receipts, { onDelete: 'CASCADE' })
  branchs: Branch;
}
