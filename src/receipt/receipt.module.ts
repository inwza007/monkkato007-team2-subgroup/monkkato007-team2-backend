import { Module } from '@nestjs/common';
import { ReceiptService } from './receipt.service';
import { ReceiptController } from './receipt.controller';
import { Receipt } from './entities/receipt.entity';
import { ReceiptDetail } from './entities/receiptDetail.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from 'src/product/entities/product.entity';
import { User } from 'src/users/entities/user.entity';
import { Branch } from 'src/branch/entities/branch.entity';
import { member } from 'src/member/entities/member.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Receipt,
      ReceiptDetail,
      Product,
      User,
      Branch,
      member,
    ]),
  ],
  controllers: [ReceiptController],
  providers: [ReceiptService],
})
export class ReceiptModule {}
