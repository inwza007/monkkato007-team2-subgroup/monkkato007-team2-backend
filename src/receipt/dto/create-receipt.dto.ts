export class CreateReceiptDto {
  receiptDetails: {
    productId: number;
    qty: number;
    // unit: number;
  }[];
  payType: string;
  amount: number;
  userId: number;
  memberId: number;
}
